![Logo Blockchain ESR](https://forgemia.inra.fr/cedric.goby/blockchain-esr/-/raw/master/source/images/logos/logo-text-horizontal-transparent.png){width=100%}

# RÉSEAU BLOCKCHAIN ENSEIGNEMENT SUPÉRIEUR ET RECHERCHE

Cette page décrit les objectifs et le fonctionnement du réseau "Blockchain ESR" (Blockchain Enseignement Supérieur et Recherche).

## QU’EST-CE QUE LA BLOCKCHAIN ?

La **blockchain** (chaîne de blocs en français) est une technologie qui permet de
garder la trace d'un ensemble de transactions (écritures dans la blockchain), de
manière décentralisée, sécurisée et transparente, sous forme d'une chaîne de blocs.

Une blockchain peut donc être assimilée à un grand registre infalsifiable.

### Fonctionnement d'une blockchain

#### Le principe de la distribution

La particularité de la blockchain est qu'elle fonctionne sans organe central de contrôle. Au lieu d'être regroupées à un seul endroit ou de passer par un seul intermédiaire, les transactions (écritures dans la blockchain) sont distribuées entre tous les serveurs des membres d'un réseau.

#### Des blocks à la blockchain

  * Lorsqu'une transaction est effectuée, elle est regroupée avec d'autres au sein d'un bloc et ne peut plus être modifiée.
  * Les mineurs valident le bloc grâce à des techniques cryptographiques.
  * Une fois le bloc validé, il est ajouté à la chaîne de blocs accessible à tous les utilisateurs. Rien ne peut être modifié ni effacé : il faudrait ajouter une nouvelle transaction en cas d'erreur.
  * La transaction est horodatée et finalisée.

![Des blocks à la blockchain](https://forgemia.inra.fr/cedric.goby/blockchain-esr/-/raw/master/source/images/blocks-blockchain.png){width=100%}

#### Les transactions

Prenons l’exemple d’une découverte scientifique majeure, non encore publiée, au format PDF. L’auteur·rice souhaite notamment garantir la propriété intellectuelle et l’antériorité de sa découverte.

Pour cela il lui suffit simplement de :
  * Créer une empreinte numérique unique (hash) du fichier PDF.
  * Enregistrer cette empreinte numérique dans la blockchain en utilisant son  adresse personnelle de portefeuille (wallet).

Il est désormais incontestable (et vérifiable) qu’à la date de l’enregistrement dans la blockchain (la transaction) ce fichier PDF, identifiable par son empreinte numérique unique, a bien été soumis par l’auteur·rice.

## LA BLOCKCHAIN POUR LA SCIENCE

**La technologie blockchain** offre une solution robuste pour garantir la
propriété intellectuelle, l'intégrité et l'authenticité des résultats scientifiques,
prévenir la falsification des données, et assurer la transparence des processus
de recherche.

  * **Sécurité**

La blockchain est un système inviolable, ce qui constitue son principal atout. Pour parvenir à pirater une blockchain, il faudrait hacker une multitude de base de données différentes (dont le nombre se compte en dizaines de milliers) et non une seule, puisque le dispositif n'est pas centralisé mais distribué.
A noter également qu'une blockchain est doublement sécurisée car une transaction nécessite deux clés. Une clé privée pour crypter une transaction, et une clé publique qui sert d'adresse pour en définir le récepteur. Chaque transaction est alors rendue infalsifiable et ne peut être modifiée.

  * **Transparence**

Tous les membres d'un réseau participent à la validation des échanges intégrés à une blockchain, et contribuent à la vérification de la fiabilité des émetteurs et récepteurs des transactions.
Cela signifie aussi qu'en cas de fraude, l'ensemble des utilisateurs serait immédiatement au courant.
Pour autant, les membres d'une blockchain n'ont pas accès à des informations qui ne les concernent pas. Les données privées restent protégées.

  * **Autonomie**

L'infrastructure d'une blockchain fonctionne en autonomie : le principe même de cette technologie supprime le tiers de confiance.
Au sein d'une blockchain, ce sont des programmes informatiques et des lignes de code qui effectuent des transactions, sans intervention. C'est notamment le cas des « smart contracts », des programmes autonomes capables d'exécuter des contrats selon des règles définies.

Par conséquent **les applications de la blockchain sont nombreuses dans le domaine de la recherche.**

  * **Propriété intellectuelle** : En offrant un registre immuable et horodaté des droits d'auteur et des licences d’utilisation, la blockchain facilite la preuve de l'antériorité d'une découverte.

  * **Authenticité** : Grâce à la blockchain, l'authenticité des données de recherche peut être vérifiée à chaque étape du processus, garantissant leur provenance.

  * **Intégrité** : Chaque transaction ou modification est enregistrée de manière transparente, empêchant les falsifications ou altérations frauduleuses.

  * **Traçabilité** : La blockchain permet de reconstituer tout l'historique des manipulations, assurant une transparence totale dans le processus scientifique.

**De nombreux cas d’usage de la blockchain peuvent ainsi être envisagés** : Attribution certaine de la propriété, antériorité, gestion des droits et des licences sur les données ou les productions scientifiques, historique du cycle de vie des données, des échantillons, des produits agricoles ou de la maintenance des équipements scientifiques, authenticité des diplômes, micro-certifications, vote électronique...

## LE RÉSEAU "BLOCKCHAIN ESR"

> "La technologie blockchain promet des transactions programmables sans intervention d'un tiers de confiance. Le potentiel disrupteur d'une telle innovation appelle une stratégie publique alliant régulation et soutien à l'innovation... sans attendre."
>
> Rapport "[Les enjeux des blockchains](https://www.strategie.gouv.fr/sites/strategie.gouv.fr/files/atoms/files/fs-rapport-blockchain-21-juin-2018.pdf)" - France Stratégie -  2018

Dans ce contexte, la création du réseau "Blockchain ESR" permet de fédérer les nombreux acteurs concernés par l'utilisation de la blockchain.

Le réseau "Blockchain ESR" est ouvert à tous les personnels ou entités de l'Enseignement Supérieur et de la Recherche, quels que soient leurs domaines d'activité, qui souhaiteraient y participer activement ou en tant que simples observateurs.

### Les objectifs du réseau "Blockchain ESR"

#### Tester des cas d'usage

Pour les premiers travaux du réseau "Blockchain ESR" nous avons retenu deux cas d'usage, actuellement en phase de test en utilisant la blockchain "[bloxberg](https://bloxberg.org/)" :

  * **Certification des données :**

La blockchain offre une solution robuste pour garantir l'authenticité et la propriété intellectuelle des jeux de données. Les chercheurs et les institutions peuvent également prouver de manière inaliénable l'antériorité d'une découverte.

De plus, grâce à des contrats intelligents, il est possible de définir des conditions d'accès, d'utilisation ou de partage de ces données, garantissant ainsi les droits d'utilisation tout en facilitant la collaboration et le partage sécurisé entre les parties.

  * **Traçabilité :**

La blockchain peut jouer un rôle crucial dans le suivi du cycle de vie des données, des échantillons, des étapes d'une expérience ou des opérations de maintenance sur des instruments scientifiques. 

Grâce à la nature immuable de cette technologie il est impossible de modifier ou de supprimer ces informations une fois qu'elles ont été enregistrées, offrant ainsi une totale traçabilité.

#### Déployer une blockchain Enseignement Supérieur et Recherche

Le réseau "Blockchain ESR" souhaite déployer une blockchain nationale opérée et utilisée par des  institutions publiques de Recherche.

Cette blockchain ESR souveraine développée par le secteur public pourra être valorisée par des acteurs privés pour développer de nouveaux produits et services, générant ainsi des opportunités économiques.

### Fonctionnement du réseau "Blockchain ESR"

#### Rejoindre le réseau

Le réseau "Blockchain ESR" est ouvert à tous les personnels ou entités de l'Enseignement Supérieur et de la Recherche au niveau international, quels que soient leurs domaines d'activité, qui souhaiteraient y participer activement ou en tant que simples observateurs.

"Blockchain ESR" est un réseau métier rattaché au département Biologie et Amélioration des Plantes [(BAP)](https://intranet.inrae.fr/bap/Vie-et-Animation-Scientifique/Reseaux) de L’institut national de recherche pour l'agriculture, l'alimentation et l'environnement [(INRAE)](https://www.inrae.fr/).

#### Membres du réseau

Le réseau "Blockchain ESR" est porté par :

**Cédric GOBY** - [cedric.goby@inrae.fr](mailto:&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#99;&#101;&#100;&#114;&#105;&#99;&#46;&#103;&#111;&#98;&#121;&#64;&#105;&#110;&#114;&#97;&#101;&#46;&#102;&#114;)  
Administrateur systèmes, Responsable Données Opérationnel (RDO)  
**INRAE** (Institut national de recherche pour l'agriculture, l'alimentation et l'environnement)  
[AGAP Institut - UMR 1334](https://umr-agap.cirad.fr/) - Équipe [Génomique évolutive et gestion des populations](https://umr-agap.cirad.fr/nos-recherches/equipes-scientifiques/genomique-evolutive-et-gestion-des-populations/contexte) (GE²pop).

**Emmanuel LANDRIVON** - [emmanuel.landrivon@ircelyon.univ-lyon1.fr](mailto:&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#101;&#109;&#109;&#97;&#110;&#117;&#101;&#108;&#46;&#108;&#97;&#110;&#100;&#114;&#105;&#118;&#111;&#110;&#64;&#105;&#114;&#99;&#101;&#108;&#121;&#111;&#110;&#46;&#117;&#110;&#105;&#118;&#45;&#108;&#121;&#111;&#110;&#49;&#46;&#102;&#114;)  
Responsable instrumentation  
**CNRS** (Centre national de la recherche scientifique)  
[IRCELyon - UMR 5256](https://www.ircelyon.univ-lyon1.fr/)

Les membres du réseau :

Sandrine Sabatié  
Ingénieure en ingénierie logicielle - INRAE, Interface [MITI-CNRS](https://miti.cnrs.fr/)

Paule Teres  
Responsable du Centre de Ressources Biologiques [GAMéT](https://umr-agap.cirad.fr/l-unite/centres-de-ressources-biologiques/crb-gamet-montpellier/contexte-et-enjeux) (Graines Adaptées aux conditions méditerranéennes et Tropicales) - Cirad, INRAE

Thomas Lallart  
Responsable [Sécurité des Systèmes d&#039;Information](https://cybersecurite.intranet.inrae.fr/) - INRAE

Franck De Wit  
Technicien Internet of Things, impression 3D [AGAP Institut](https://umr-agap.cirad.fr/) - INRAE, Cirad, Institut AGRO, Université de Montpellier

Martin K. Amouzou  
Ingénieur de données, réseau [ARDoISE](https://recherche.data.gouv.fr/fr/page/ardoise-atelier-rennais-de-la-donnee-information-et-soutien-aux-equipes-de-recherche) - Université Rennes 2, Université de Rennes

Micaël Aliouat  
Juriste Droit du numérique et LegalTech au Pôle Droit de la Recherche de la [Direction des affaires juridiques](https://intranet.inrae.fr/daj/) - INRAE

Daniel Jacob  
Ingénieur de Recherche, développeur logiciel [Maggot (Matadata aggregation on data storage)](https://inrae.github.io/pgd-mmdt/)  - INRAE

Jonathan MINEAU-CESARI  
Data Steward - Administrateur Système d'Information, INRAE Direction pour la Science Ouverte [(DipSO)](https://science-ouverte.inrae.fr/fr/la-science-ouverte/la-direction-pour-la-science-ouverte)

#### Outils de travail collaboratif

Des outils de travail collaboratif seront progressivement mis à disposition des membres du réseau, avec dès à présent :

  * Une liste de diffusion RENATER : [https://groupes.renater.fr/sympa/info/blockchain-esr](https://groupes.renater.fr/sympa/info/blockchain-esr)  accessible via la Fédération d'Identité.
  * Un dépôt pour les codes sources et la documentation : [https://forgemia.inra.fr/cedric.goby/blockchain-esr](https://forgemia.inra.fr/cedric.goby/blockchain-esr)



![Logos partenaires](source/images/logos/cartouche-partenaires-blockchain.png){width=100%}